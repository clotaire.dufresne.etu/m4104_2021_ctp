package fr.ulille.iutinfo.teletp;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class SuiviAdapter extends RecyclerView.Adapter<SuiviAdapter.ViewHolder>/* TODO Q6.a */ {
    private SuiviViewModel model;

    // TODO Q6.a
    public SuiviAdapter(SuiviViewModel model) {
        this.model = model;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.question_view, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.tvQuestion.setText(model.getQuestions(position));
    }

    @Override
    public int getItemCount() {
        return model.getNbQuestions();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvQuestion;
        CheckBox checkBox;
        ViewHolder(View itemView) {
            super(itemView);
            tvQuestion = itemView.findViewById(R.id.question);
            checkBox = itemView.findViewById(R.id.checkBox);
        }
    }
    // TODO Q7
}
