package fr.ulille.iutinfo.teletp;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.fragment.NavHostFragment;

import java.lang.reflect.Array;

public class VueGenerale extends Fragment {

    // TODO Q1
    private String salle, poste;
    private String DISTANCIEL;
    // TODO Q2.c
    private SuiviViewModel model;

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.vue_generale, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // TODO Q1
        this.DISTANCIEL = getResources().getStringArray(R.array.list_salles)[0];
        this.poste = "";
        this.salle = DISTANCIEL;
        // TODO Q2.c
        model = new ViewModelProvider(requireActivity()).get(SuiviViewModel.class);
        // TODO Q4
        Spinner spSalle = view.findViewById(R.id.spSalle);
        ArrayAdapter<CharSequence> ad1 = ArrayAdapter.createFromResource(getContext(), R.array.list_salles, R.layout.support_simple_spinner_dropdown_item);
        spSalle.setAdapter(ad1);

        Spinner sp2 = view.findViewById(R.id.spPoste);
        ArrayAdapter<CharSequence> ad2 = ArrayAdapter.createFromResource(getContext(), R.array.list_postes, R.layout.support_simple_spinner_dropdown_item);
        sp2.setAdapter(ad2);



        view.findViewById(R.id.btnToListe).setOnClickListener(view1 -> {
            // TODO Q3
            TextView tvLogin = getActivity().findViewById(R.id.tvLogin);
            model.setUsername(tvLogin.getText().toString());
            NavHostFragment.findNavController(VueGenerale.this).navigate(R.id.generale_to_liste);
        });

        // TODO Q5.b
        spSalle.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                salle = getContext().getResources().getStringArray(R.array.list_salles)[position];
                update();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                update();
            }
        });

        sp2.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                poste = getContext().getResources().getStringArray(R.array.list_postes)[position];
                update();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                update();
            }
        });

        update();
        // TODO Q9
    }

    // TODO Q5.a
    public void update() {
        Spinner spPoste = getActivity().findViewById(R.id.spPoste);
        if(salle.equals(DISTANCIEL)) {
            spPoste.setVisibility(View.INVISIBLE);
            spPoste.setEnabled(false);
            model.setLocalisation(DISTANCIEL);
        }
        else {
            spPoste.setVisibility(View.VISIBLE);
            spPoste.setEnabled(true);
            model.setLocalisation(salle+" : "+poste);
        }

    }
    // TODO Q9
}